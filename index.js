#!/usr/bin/env node --harmony

var readDir = require("recursive-readdir")
var fs = require('fs')

async function getFilesInDir(dir){
	return new Promise((resolve)=>{
		readDir(dir,(err, files)=>{
			resolve(files);
		})
	})
}

function extractTextByPrefix(file, prefix){
	let content = fs.readFileSync(file,{encoding:'utf8'});
	let regex = new RegExp('{{'+prefix+'.(.*?)}}', 'g')
	let words = [];
	let matches;
	while((matches = regex.exec(content)) !== null){
		if (matches.length > 0) matches[1]
		let word = matches[1];
		words.push(word);
	}
	return words;
}


function outputAsCsv(array, outputFileName){
	let csv = '"_keys",'+"\n";
	array.map((key)=>{
		csv += '"'+key+'",'+"\n"
	})
	fs.writeFileSync(outputFileName, csv)
}

function mergeArray(sourceArray, newArray){
	return sourceArray.concat(newArray).filter((x, i, a) => a.indexOf(x) == i);
}

async function main(args){
	let srcDir = args[0] || 'src'
	let prefix = args[1] || 'appVm.texts'
	let outputFileName = args[2] || 'output.csv'

	let files = await getFilesInDir(srcDir);
	let allExtractedTexts = [];
	for(let i in files){
		let file = files[i];
		let extractedTexts = extractTextByPrefix(file, prefix);
		allExtractedTexts = mergeArray(allExtractedTexts, extractedTexts);
	}
	outputAsCsv(allExtractedTexts, outputFileName);
}

main(process.argv.slice(2));
