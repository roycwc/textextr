# Textextr
Extract {{text.*}} from a directory and export a .csv file containing all *
Useful for i18n

# Requirements
node v7.0+

# Usage
`textextr [dir] [prefix] [outputFileName]`

```
npm install -g textextr
textextr ./src vm.text. output.csv
```
